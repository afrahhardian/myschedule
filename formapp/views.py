from django.http import HttpResponseRedirect
from django.shortcuts import render
from formapp.forms import NameForm
from formapp.models import Activity

def form_display(request):
    acitvities = Activity.objects.all().values()
    response = {'activities': acitvities }
    return render(request, 'formapp/form_display.html',response)

def get_name(request):
    # if this is a POST request we need to process the form data

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # response['your_date'] = request.POST['your_date']
            # response['your_kegiatan'] = request.POST['your_kegiatan']
            # response['your_place'] = request.POST['your_place']
            # response['your_kategori'] = request.POST['your_kategori']
            # redirect to a new URL:
            # your_date = request.POST['your_date']
            # your_kegiatan = request.POST['your_kegiatan']
            # your_place = request.POST['your_place']
            # your_kategori = request.POST['your_kategori']
            # activity = Activity(your_date = response['your_date'], your_kegiatan = response['your_kegiatan'], your_place = response['your_place'], your_kategori = response['your_kategori'])
            form.save()

            form2  = NameForm()
            return render(request, 'formapp/name.html', {'form': form2})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'formapp/name.html', {'form': form})

def deleteAll(request):
    act = Activity.objects.all()
    act.delete()
    return HttpResponseRedirect('/form_display/')
