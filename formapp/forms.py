from django import forms
from formapp.models import Activity

class NameForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = ['your_date','your_time', 'your_kegiatan', 'your_place', 'your_kategori']
        widgets = {
            'your_date': forms.DateInput(attrs = {'type': 'date','class' : 'form-control'}),
            'your_time': forms.TimeInput(attrs = {'type': 'time', 'class' : 'form-control'}),
            'your_kegiatan': forms.TextInput(attrs = {'class' : 'form-control'}),
            'your_place': forms.TextInput(attrs = {'class' : 'form-control'}),
            'your_kategori': forms.TextInput(attrs = {'class' : 'form-control'}),
        }

        labels = {
            'your_date': 'Date',
            'your_time': 'Time',
            'your_kegiatan': 'Activity Name',
            'your_place' : 'Place',
            'your_kategori': "Category"
        }

    # class_attrs = {
    #     'class' : 'form-control',
    # }
    # your_date = forms.DateTimeField(label = 'Day', widget = forms.DateTimeInput(class_attrs))
    # your_kegiatan = forms.CharField(label = 'Activity',widget = forms.TextInput(class_attrs))
    # your_place = forms.CharField(label = 'Place', widget = forms.TextInput(class_attrs))
    # your_kategori = forms.CharField(label = 'Category', widget = forms.TextInput(class_attrs))
