# Generated by Django 2.1.1 on 2018-10-03 15:34

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('your_date', models.DateField()),
                ('your_time', models.TimeField()),
                ('your_kegiatan', models.TextField(max_length=100)),
                ('your_place', models.TextField(max_length=100)),
                ('your_kategori', models.TextField(max_length=100)),
            ],
        ),
    ]
