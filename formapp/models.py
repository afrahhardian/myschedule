from django.db import models
from django.utils import timezone
from datetime import datetime, date



# Create your models here.
class Activity(models.Model):
    your_date = models.DateField()
    your_time = models.TimeField()
    your_kegiatan = models.TextField(max_length = 100)
    your_place = models.TextField(max_length = 100)
    your_kategori = models.TextField(max_length = 100)

    class meta:
        ordering = ['your_date','your_time']
