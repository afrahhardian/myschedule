from django.urls import path
from . import views

urlpatterns = [
    path('', views.get_name, name = "get_name"),
    path('Home/', views.get_name, name  = "get_name"),
    path('form_display/', views.form_display, name = "form_display"),
    path('deleteAll/', views.deleteAll, name= 'deleteAll')
]
